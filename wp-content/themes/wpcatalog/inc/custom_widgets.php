<?php

class CategoriesThemesWidget extends WP_Widget {

	/*
	 * создание виджета
	 */
	function __construct() {
		parent::__construct(
			'categories_themes_widget',
			'Категории тем', // заголовок виджета
			array( 'description' => 'Выводит категории тем' ) // описание
		);
	}

	/*
	 * фронтэнд виджета
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] ); // к заголовку применяем фильтр (необязательно)

		echo $args['before_widget'];

		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$terms = get_terms( array(
			'hide_empty' => 0,
			'orderby'    => 'name',
			'order'      => 'ASC',
			'taxonomy'   => 'category-theme',
			'pad_counts' => 1
		) );
		?>

        <ul class="footer-categories">

			<?php
			foreach ( $terms as $term ) {
				$term_link  = get_term_link( (int) $term->term_id, 'category-theme' );
				?>

                <li class="footer-item">
                    <a href="<?php echo $term_link; ?>"><?php echo $term->name; ?></a>
                </li>

			<?php } ?>

        </ul>

		<?php
		echo $args['after_widget'];
	}

	/*
	 * бэкэнд виджета
	 */
	public function form( $instance ) {
		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		}

		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Заголовок</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
                   name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
                   value="<?php echo esc_attr( $title ); ?>"/>
        </p>
		<?php
	}

	/*
	 * сохранение настроек виджета
	 */
	public function update( $new_instance, $old_instance ) {
		$instance          = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}
}

/*
 * регистрация виджета
 */
function wpcatalog_widget_load() {
	register_widget( 'CategoriesThemesWidget' );
}

add_action( 'widgets_init', 'wpcatalog_widget_load' );