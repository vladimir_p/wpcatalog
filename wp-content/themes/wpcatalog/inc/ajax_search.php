<?php

// ajax поиск по сайту
add_action( 'wp_ajax_nopriv_ajax_search', 'ajax_search' );
add_action( 'wp_ajax_ajax_search', 'ajax_search' );
function ajax_search() {
    $search = sanitize_text_field($_POST['search']);
    $slug   = sanitize_text_field($_POST['slug']);
	$args_category = array();

	if ( $slug ) {
		$args_category['tax_query'][] = array(
			'taxonomy' => 'category-theme',
			'field'    => 'slug',
			'terms'    => $slug
		);
    }

    $args  = array(
		'post_type'      => 'theme',
		'post_status'    => 'publish',
		'order'          => 'DESC',
		'orderby'        => 'date',
		's'              => $search,
		'posts_per_page' => 5
	);

	$args = array_merge($args, $args_category);

	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post(); ?>

            <li class="search-content">
                <a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail(); ?>
                    <div class="search-info">
                        <h4 class="search-title"><?php the_title(); ?></h4>
						<?php the_excerpt(); ?>
                    </div>
                </a>
            </li>

		<?php }
	} else { ?>

        <li class="search-content">Ничего не найдено, попробуйте другой запрос</li>

		<?php
	}

	wp_die();
}