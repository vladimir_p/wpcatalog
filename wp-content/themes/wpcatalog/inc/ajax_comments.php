<?php

add_action('wp_ajax_wpcatalog_comments', 'ajax_comments');
add_action('wp_ajax_nopriv_wpcatalog_comments', 'ajax_comments');
function ajax_comments() {

	// пробуем записать коммент в БД
	$comment = wp_handle_comment_submission( wp_unslash($_POST['data']) );

	// ошибка при добавлении комментария
	if( is_wp_error( $comment ) ) {
		wp_send_json_error( $comment->get_error_message() );
		wp_die();
	}

    ?>

    <li <?php comment_class('' , null, null, true); ?> id="comment-<?php echo $comment->comment_ID; ?>">
        <div id="div-comment-<?php echo $comment->comment_ID; ?>" class="comment-body">

			<?php if ( $comment->comment_approved == '0' ) { ?>
                <div class="comment-awaiting-moderation">
					<?php _e( 'Your comment is awaiting moderation.' ); ?>
                </div>
			<?php } ?>

            <div class="comment-wrap">
                <div class="comment-avatar">
					<?php echo get_avatar( $comment ); ?>
                </div>
                <div class="comment-box">
                    <div class="comment-head">
                        <div class="comment-info">
                            <h6 class="comment-name"><?php echo get_comment_author_link( $comment ); ?></h6>
                            <span>
                        <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
                            <?php echo get_comment_date( '', $comment ); ?>
                        </a>
					</span>
                        </div>
                    </div>
                    <div class="comment-content"><?php comment_text( $comment->comment_ID ); ?></div>
					<?php edit_comment_link( __( '(Edit)' ), '  ', '' ); ?>
                </div>
            </div>
        </div>
    </li>

    <?php

	wp_die();
}