<?php

add_action( 'wp_ajax_wpcatalog_sorting_themes', 'ajax_sorting_themes' );
add_action( 'wp_ajax_nopriv_wpcatalog_sorting_themes', 'ajax_sorting_themes' );

function ajax_sorting_themes() {
	if ( isset( $_POST['sorting'] ) ) {

		$slug    = sanitize_text_field( $_POST['slug'] );
		$sorting = sanitize_text_field( $_POST['sorting'] );
		$slug_arr = explode('/', $slug);

		if ( array_search('theme', $slug_arr) ) {
			$slug = '';
			$url = 'theme';
		} else {
			$slug = $slug_arr['0'];
			$url = 'category-theme/' . $slug;
		}

		switch ( $sorting ) {
			case 'date':
				$theme_sorting  = '';
				$meta_value_num = 'date';
				break;
			case 'views':
				$theme_sorting  = 'theme_views';
				$meta_value_num = 'meta_value_num';
				break;
			case 'download':
				$theme_sorting  = 'theme_download';
				$meta_value_num = 'meta_value_num';
				break;
		}

		$args = array(
			'post_status'    => 'publish',
			'post_type'      => 'theme',
			'meta_key'       => $theme_sorting,
			'orderby'        => $meta_value_num,
			'category-theme' => $slug,
			'paged' => get_query_var('paged') ?: 1
		);

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();

				get_template_part( 'template-parts/content', 'single' );
			}
		}

		wp_reset_postdata();

		paginate_ajax($query, $url, $sorting);
	}
	wp_die();
}