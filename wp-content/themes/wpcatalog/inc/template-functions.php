<?php

/* Подсчет количества просмотра темы
---------------------------------------------------------- */
add_action( 'wp_head', 'theme_views' );
function theme_views() {

	/* ------------ Настройки -------------- */
	$meta_key     = 'theme_views';  // Ключ мета поля, куда будет записываться количество просмотров.
	$who_count    = 1;              // Чьи посещения считать? 0 - Всех. 1 - Только гостей. 2 - Только зарегистрированных пользователей.
	$exclude_bots = 1;              // Исключить ботов, роботов, пауков и прочую нечесть :)? 0 - нет, пусть тоже считаются. 1 - да, исключить из подсчета.

	global $user_ID, $post;
	if ( is_singular( 'theme' ) ) {
		$id = (int) $post->ID;

		static $theme_views = false;
		if ( $theme_views ) {
			return true;
		} // чтобы 1 раз за поток
		$theme_views  = (int) get_post_meta( $id, $meta_key, true );
		$should_count = false;
		switch ( (int) $who_count ) {
			case 0:
				$should_count = true;
				break;
			case 1:
				if ( (int) $user_ID == 0 ) {
					$should_count = true;
				}
				break;
			case 2:
				if ( (int) $user_ID > 0 ) {
					$should_count = true;
				}
				break;
		}
		if ( (int) $exclude_bots == 1 && $should_count ) {
			$useragent = $_SERVER['HTTP_USER_AGENT'];
			$notbot    = "Mozilla|Opera"; //Chrome|Safari|Firefox|Netscape - все равны Mozilla
			$bot       = "Bot/|robot|Slurp/|yahoo"; //Яндекс иногда как Mozilla представляется
			if ( ! preg_match( "/$notbot/i", $useragent ) || preg_match( "!$bot!i", $useragent ) ) {
				$should_count = false;
			}
		}

		if ( $should_count ) {
			if ( ! update_post_meta( $id, $meta_key, ( $theme_views + 1 ) ) ) {
				add_post_meta( $id, $meta_key, 1, true );
			}
		}
	}

	return true;
}

// показывает количество просмотров темы
function get_theme_views( $theme_id ) {
	$theme_views = get_post_meta( $theme_id, 'theme_views', true );
    echo $theme_views;
}

// показывает количество просмотров темы
function get_theme_download( $theme_id ) {
	$theme_download = get_post_meta( $theme_id, 'theme_download', true );
	echo $theme_download;
}

// Удаляет поле "Сайт" из формы комментирования для незарегистрированных пользователей.
add_filter( 'comment_form_default_fields', 'comment_form_remove_default_fields' );
function comment_form_remove_default_fields( $fields ) {
	unset( $fields['url'] );

	return $fields;
}

// меняет порядок полей в форме комментария
add_filter( 'comment_form_fields', 'reorder_comment_fields' );
function reorder_comment_fields( $fields ) {

	$new_fields = array();

	$order_fields = array( 'author', 'email', 'comment' );

	foreach ( $order_fields as $key ) {
		$new_fields[ $key ] = $fields[ $key ];
		unset( $fields[ $key ] );
	}

	// если остались еще какие-то поля добавим их в конец
	if ( $fields ) {
		foreach ( $fields as $key => $val ) {
			$new_fields[ $key ] = $val;
		}
	}

	return $new_fields;
}

// callback список комментариев
function wpcatalog_comment( $comment, $args, $depth ) {
	if ( 'div' === $args['style'] ) {
		$tag       = 'div';
		$add_below = 'comment';
	} else {
		$tag       = 'li';
		$add_below = 'div-comment';
	}

	$classes = ' ' . comment_class( empty( $args['has_children'] ) ? '' : 'parent', null, null, false );
	?>

    <<?php echo $tag, $classes; ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) { ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body"><?php
	} ?>

	<?php /* if ( $comment->comment_approved == '0' ) { ?>
		<div class="comment-awaiting-moderation">
			<?php _e( 'Your comment is awaiting moderation.' ); ?>
		</div>
	<?php } */ ?>

    <div class="comment-wrap">
        <div class="comment-avatar">
			<?php
			if ( $args['avatar_size'] != 0 ) {
				echo get_avatar( $comment, $args['avatar_size'] );
			}
			?>
        </div>
        <div class="comment-box">
            <div class="comment-head">
                <div class="comment-info">
                    <h6 class="comment-name"><?php echo get_comment_author_link(); ?></h6>
                    <span>
                    <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
                        <?php
                        printf(
	                        __( '%1$s at %2$s' ),
	                        get_comment_date(),
	                        get_comment_time()
                        );
                        ?>
                    </a>
                </span>
                </div>
                <div class="reply">
					<?php
					comment_reply_link(
						array_merge(
							$args,
							array(
								'add_below'  => $add_below,
								'depth'      => $depth,
								'max_depth'  => $args['max_depth'],
								'reply_text' => '<i class="fa fa-reply"></i>'
							)
						)
					);

					?>
                </div>
            </div>
            <div class="comment-content"><?php comment_text(); ?></div>
			<?php edit_comment_link( __( '(Edit)' ), '  ', '' ); ?>
        </div>
    </div>

	<?php if ( 'div' != $args['style'] ) { ?>
        </div>
	<?php }
}

// добавляет блок для вывода ошибок комментариев
add_action( 'comment_form_top', 'comment_form_message_error' );
function comment_form_message_error() {
	echo '<div id="comment-error" class="comment-error"></div>';
}

// хлебные крошки
function breadcrumbs() {

	if ( ! is_home() ) {

		echo '<li><a href="' . site_url() . '">Главная</a></li>
		      <li><i class="fa fa-chevron-right" aria-hidden="true"></i></li> ';

		if ( is_single() || is_page() ) { // записи

			echo "<li>" . get_the_title() . "</li>";

		} elseif ( is_tax() ) { // страницы таксономии

			echo '<li>' . single_cat_title( '', false ) . '</li>';

		} elseif ( is_search() ) { // страницы поиска

			echo '<li>Поиск</li>';

		} elseif ( is_404() ) { // если страницы не существует

			echo 'Ошибка 404';

		}

	} else { // главная

		$pageNum = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		if ( $pageNum > 1 ) {
			echo '<a href="' . site_url() . '">Главная</a> &amp;raquo; ' . $pageNum . '-я страница';
		} else {
			echo 'Вы находитесь на главной странице';
		}
	}
}

// добавляет поля theme_views и theme_download при сохранении темы
add_action( 'save_post', 'add_fields_theme' );
function add_fields_theme( $post_id ) {
	if ( wp_is_post_revision( $post_id ) ) {
		return;
	}

	$theme_download = get_post_meta( $post_id, 'theme_download', true );
	$theme_views = get_post_meta( $post_id, 'theme_views', true );

	if (!$theme_download) {
		add_post_meta( $post_id, 'theme_download', 0, true );
    }

	if (!$theme_views) {
		add_post_meta( $post_id, 'theme_views', 0, true );
	}

}

// пагинация
function pagination() {
	$args = array(
		'show_all'           => false, // показаны все страницы участвующие в пагинации
		'end_size'           => 1,     // количество страниц на концах
		'mid_size'           => 2,     // количество страниц вокруг текущей
		'prev_next'          => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
		'prev_text'          => __( '<i class="fa fa-chevron-left"></i>' ),
		'next_text'          => __( '<i class="fa fa-chevron-right"></i>' ),
		'add_args'           => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
		'add_fragment'       => '',     // Текст который добавиться ко всем ссылкам.
	);

	the_posts_pagination( $args );
}

// удаляет H2 из шаблона пагинации
add_filter( 'navigation_markup_template', 'navigation_template', 10, 2 );
function navigation_template() {

	return '
	<nav class="navigation %1$s">
		<div class="nav-links">%3$s</div>
	</nav>    
	';
}

// создаем новую колонку просмотров
add_filter( 'manage_theme_posts_columns', 'add_theme_views_column', 4 );
function add_theme_views_column( $columns ){
	$num = 2; // после какой по счету колонки вставлять новые

	$new_columns = array(
		'theme_views' => 'Просмотры',
	);

	return array_slice( $columns, 0, $num ) + $new_columns + array_slice( $columns, $num );
}

// заполняем колонку просмотров данными
add_action('manage_theme_posts_custom_column', 'fill_theme_views_column', 5, 2 );
function fill_theme_views_column( $colname, $post_id ){
	if( $colname === 'theme_views' ){
		echo get_post_meta( $post_id, 'theme_views', 1 );
	}
}

// добавляем возможность сортировать колонку просмотров
add_filter('manage_edit-theme_sortable_columns', 'add_theme_views_sortable_column');
function add_theme_views_sortable_column($sortable_columns){
	$sortable_columns['theme_views'] = 'theme_views';

	return $sortable_columns;
}

// изменяем запрос при сортировке колонки просмотров
add_filter('pre_get_posts', 'add_column_theme_views_request');
function add_column_theme_views_request( $object ){
	if( $object->get('orderby') != 'theme_views' )
		return;

	$object->set('meta_key', 'theme_views');
	$object->set('orderby', 'meta_value_num');
}


// создаем новую колонку закачек
add_filter( 'manage_theme_posts_columns', 'add_theme_download_column', 4 );
function add_theme_download_column( $columns ){
	$num = 3; // после какой по счету колонки вставлять новые

	$new_columns = array(
		'theme_download' => 'Закачки',
	);

	return array_slice( $columns, 0, $num ) + $new_columns + array_slice( $columns, $num );
}

// заполняем колонку закачек данными
add_action('manage_theme_posts_custom_column', 'fill_theme_download_column', 5, 2 );
function fill_theme_download_column( $colname, $post_id ){
	if( $colname === 'theme_download' ){
		echo get_post_meta( $post_id, 'theme_download', 1 );
	}
}

// добавляем возможность сортировать колонку закачек
add_filter('manage_edit-theme_sortable_columns', 'add_theme_download_sortable_column');
function add_theme_download_sortable_column($sortable_columns){
	$sortable_columns['theme_download'] = 'theme_download';

	return $sortable_columns;
}

// изменяем запрос при сортировке колонки закачек
add_filter('pre_get_posts', 'add_column_theme_download_request');
function add_column_theme_download_request( $object ){
	if( $object->get('orderby') != 'theme_download' )
		return;

	$object->set('meta_key', 'theme_download');
	$object->set('orderby', 'meta_value_num');
}

// пагинация для сортировки на странице категорий и каталога при ajax
function paginate_ajax($query, $url, $sorting) {

	$args = array(
		'base'               => home_url( $url . '/%_%' ),
        'format'             => 'page/%#%/?sorting=' . $sorting,
		'total'              => $query->max_num_pages,
		'current'            => max( 1, get_query_var('paged') ),
		'show_all'           => false,
		'end_size'           => 1,
		'mid_size'           => 2,
		'prev_next'          => true,
		'prev_text'          => __( '<i class="fa fa-chevron-left"></i>' ),
		'next_text'          => __( '<i class="fa fa-chevron-right"></i>' ),
		'type'               => 'plain',
		'add_args'           => false,
		'add_fragment'       => '',
		'before_page_number' => '',
		'after_page_number'  => ''
	);

	?>
    <div class="col-md-12">
        <nav class="pagination">
            <div class="nav-links">
				<?php echo paginate_links( $args ) ;?>
            </div>
        </nav>
    </div>
    <?php
}

function add_css_class( $value ) {
	if ( isset( $_GET['sorting'] ) && $_GET['sorting'] === $value ) {
	    echo 'active';
	}
}

// добавляет alt картинке при загрузке в менеджер файлов
add_action( 'add_attachment', 'add_image_alt' );

function add_image_alt( $attachment_ID ) {
	update_post_meta($attachment_ID, '_wp_attachment_image_alt', get_the_title($attachment_ID) );
}