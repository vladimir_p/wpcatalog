<?php

// Тип записи "Тема"
add_action('init', 'register_post_type_theme');
function register_post_type_theme(){
	register_post_type('theme', array(
		'labels'             => array(
			'name'               => 'Темы', // Основное название типа записи
			'singular_name'      => 'Тема', // отдельное название записи типа Book
			'add_new'            => 'Добавить новую',
			'add_new_item'       => 'Добавить новую тему',
			'edit_item'          => 'Редактировать тему',
			'new_item'           => 'Новая тема',
			'view_item'          => 'Посмотреть тему',
			'search_items'       => 'Найти тему',
			'not_found'          =>  'Тем не найдено',
			'not_found_in_trash' => 'В корзине тем не найдено',
			'parent_item_colon'  => '',
			'menu_name'          => 'Темы'

		),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 4,
		'supports'           => array('title','editor','author','thumbnail','excerpt','comments')
	) );
}