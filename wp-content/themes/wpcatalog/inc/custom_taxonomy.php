<?php

// таксономия "Категория" для типа записи "Тема"
add_action( 'init', 'create_theme_taxonomy' );

function create_theme_taxonomy(){

	register_taxonomy('category-theme', 'theme', array(
		'hierarchical'  => true,
		'labels'        => array(
			'name'              => 'Категории',
			'singular_name'     => 'Категория',
			'search_items'      => 'Найти категорию',
			'all_items'         => 'Все категории',
			'parent_item'       => 'Родительская категория',
			'edit_item'         => 'Редактировать категорию',
			'update_item'       => 'Обновить категорию',
			'add_new_item'      => 'Добавить категорию',
			'new_item_name'     => 'Новая категория',
			'menu_name'         => 'Категория',
		),
		'show_ui'       => true,
		'query_var'     => true,
		//'rewrite'       => array( 'slug' => 'the_genre' ), // свой слаг в URL
	));
}

// получаеи иконкe категории
function get_icon_category_theme($category) {
	return get_term_meta( $category->term_id, 'icon_class', true );
}