<?php

add_action( 'wp_ajax_wpcatalog_download', 'ajax_download' );
add_action( 'wp_ajax_nopriv_wpcatalog_download', 'ajax_download' );
function ajax_download() {

	if ( $_POST['download'] ) {
		$theme_download = (int) get_post_meta( $_POST['post_id'], 'theme_download', true );
		update_post_meta( $_POST['post_id'], 'theme_download', ( $theme_download + 1 ) );
	}

	wp_die();
}