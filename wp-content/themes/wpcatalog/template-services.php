<?php
/*
 * Template name: Услуги
 */

get_header();
get_template_part( 'template-parts/breadcrumbs' ); ?>

	<div class="page-content">
		<div class="container main-content">
			<main class="main">

				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', 'services' );

				endwhile;
				?>

			</main>
		</div>
	</div>

<?php get_footer();

