<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package wpcatalog
 */

get_header(); ?>

    <div class="page-content">
        <div class="container main-content">
            <main class="main">

                <section class="error-404 not-found">
                    <header class="page-header">
                        <h1 class="page-title">Страница не найдена</h1>
                    </header><!-- .page-header -->
                    <div class="page-content">
                        <p>Пожалуйста, перейдите на <a href="/">гланую</a> или в <a href="/theme">каталог</a></p>
                    </div><!-- .page-content -->
                </section><!-- .error-404 -->

            </main><!-- #main -->
        </div>
    </div>

<?php
get_footer();
