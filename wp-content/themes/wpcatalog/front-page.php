<?php
get_header();

$banner_img         = get_field( 'banner_img' );
$banner_title       = get_field( 'banner_title' );
$banner_subtitle    = get_field( 'banner_subtitle' );
$popular_categories = get_field( 'popular_categories' );

?>
    <section class="banner">
        <div class="banner-img" style="background: url(<?php echo $banner_img['url']; ?>) center;"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="banner-title"><?php echo $banner_title; ?></h1>
                    <p class="banner-desc"><?php echo $banner_subtitle; ?></p>
                    <div class="search-themes">
	                    <?php get_search_form(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="popular-categories">
        <div class="container">
            <div class="row">
                <h3 class="block-title line">Популярные категории</h3>
                <ul class="themes-list">
					<?php foreach ( $popular_categories as $category ) {

						$term_link  = get_term_link( (int) $category->term_id, 'category-theme' );
						$icon_class = get_icon_category_theme( $category );
						?>
                        <li class="themes-item">
                            <a class="category-link" href="<?php echo $term_link; ?>">
                                <i class="fa <?php echo $icon_class; ?>"></i>
                                <h3 class="category-name"><?php echo $category->name; ?></h3>
                            </a>
                        </li>
					<?php } ?>
                </ul>
            </div>
        </div>
    </section>
    <div class="container main-content">
        <main class="main">
            <h3 class="block-title line">Последние добавленные темы</h3>
            <div class="row">
                <?php

                $args = array(
	                'post_type' => 'theme',
	                'posts_per_page' => 6
                );

                $query = new WP_Query( $args );

                if ( $query->have_posts() ) {
	                while ( $query->have_posts() ) {
		                $query->the_post();

		                    get_template_part( 'template-parts/content', 'single' );
	                }
                }

                wp_reset_postdata();

                ?>

            </div>
            <a class="button button-catalog" href="/theme">Каталог</a>
        </main>
    </div>

<?php get_footer();
