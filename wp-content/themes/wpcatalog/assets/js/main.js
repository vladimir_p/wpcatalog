( function ($) {
    // поиск тем
    $('.select-category').click(function () {
        $(this).parent().toggleClass('active');
        $('.search-result').removeClass('active')
    });

    // добавляет класи для изменения размеров template-device
    function size_iframe(selector, remove_class_1, remove_class_2, add_class) {
        var template_show = $('#template-device');
        $(selector).click(function () {
            setTimeout(function () {
                template_show.removeClass(remove_class_1);
                template_show.removeClass(remove_class_2);
                template_show.addClass(add_class);
            }, 500);
        });
    }

    // monitor
    size_iframe('.device-selections .fa-desktop', 'device-tablet', 'device-mobile', 'device-monitor');

    // tablet
    size_iframe('.device-selections .fa-tablet', 'device-monitor', 'device-mobile', 'device-tablet');

    // mobile
    size_iframe('.device-selections .fa-mobile', 'device-tablet', 'device-monitor', 'device-mobile');

    // плавное появление iframe, запрет повторного появления iframe при нажатии иконки
    $('.device-selections i').click(function () {
        if (!$(this).hasClass('active')) {
            $('.device-selections i').removeClass('active');
            $(this).addClass('active');
            $('#template-device').addClass('device-hide');
            setTimeout(function () {
                $('#template-device').removeClass('device-hide');
            }, 1000);
        }
    });

    // форма комментария
    $('.comment-form input, .comment-form textarea').focusout(function () {
        var input = $(this).val();

        if (input === "") {
            $(this).removeClass('has-value');
        } else {
            $(this).addClass('has-value');
        }

    });

    // мобильное меню
    $('#mobile-icon').click(function () {
        $(this).siblings().toggleClass('open');
        $('body').toggleClass('menu-scroll');
    });

    $(".menu-item-has-children").click(function () {
        $(this).toggleClass("active");
    });

    $(window).bind("resize", function () {

        // добавляет клас к меню при экране 992px
        if ($(this).width() < 992) {
            $('.menu').addClass('mobile-menu');
        } else {
            $('.menu').removeClass('mobile-menu');
        }

        // изменяет высоту "monitor, tablet" в зависимости от экрана монитора
        if ($(this).width() < 768) {
            var heights = 768 / $(this).width();
            console.log(500 / heights);
            $('.device-monitor, .device-tablet').css("height", 500 / heights);
        } else if ($(this).width() < 992) {
            $('.device-monitor, .device-tablet').css("height", 500);
        } else {
            $('.preview-template').css("height", 600);
        }

        $(".device-selections .fa-mobile").click(function (event) {
            $('.preview-template').css("height", "");
        });

    }).trigger('resize');

    // комментарии
    $("#comment-submit").click(function (event) {
        event.preventDefault();
        var comment = $('#comment').val();
        var author = $('#author').val();
        var email = $('#email').val();
        var comment_post_ID = $('#comment_post_ID').val();
        var comment_parent = $('#comment_parent').val();
        $.ajax({
            method: 'POST',
            url: wpcatalog.ajaxurl,
            data: {
                action: 'wpcatalog_comments',
                data: {
                    comment: comment,
                    author: author,
                    email: email,
                    comment_post_ID: comment_post_ID,
                    comment_parent: comment_parent
                }
            },
            success: function (comment) {
                if (comment.success !== false) {
                    // Если уже есть какие-то комментарии
                    console.log($('.comments-list').length);
                    if ($('.comments-list').length > 0) {
                        // Если текущий комментарий является ответом
                        if ($('#respond').parent().hasClass('comment')) {
                            // Если уже есть какие-то ответы
                            if ($('#respond').parent().children('.children').length) {
                                $('#respond').parent().children('.children').append(comment);
                            } else {
                                // Если нет, то добавляем  <ul class="children">
                                comment = '<ul class="children">' + comment + '</ul>';
                                $('#respond').parent().append(comment);
                            }
                            // закрываем форму ответа
                            $("#cancel-comment-reply-link").trigger("click");
                        } else {
                            // обычный коммент
                            $('.comments-list').append(comment);
                        }
                    } else {
                        // если комментов пока ещё нет, тогда
                        comment = '<ul class="comments-list">' + comment + '</ul>';
                        $('#respond').before($(comment));
                    }
                    // очищаем поле textarea
                    $('#comment').val('');

                } else {
                    $('#comment-error').html(comment.data);
                }
            },
        });
    });

    // удаляет текст ошибки для формы комментария
    $('.comment-form input, .comment-form textarea').focus(function () {
        $('#comment-error').html('');
    });

    // скачивание темы
    $('#theme-download').click(function () {
        var post_id = $(this).data('post-id');
        $.ajax({
            method: 'POST',
            url: wpcatalog.ajaxurl,
            data: {
                action: 'wpcatalog_download',
                download: true,
                post_id: post_id
            }
        });
    });

    // сортировка тем
    $('#sorting-themes a').click(function (event) {
        event.preventDefault();

        var pathname = window.location.pathname;
        var category_slug = pathname.replace('/category-theme/', '');

        if (!$(this).hasClass('active')) {
            $('#sorting-themes a').removeClass('active');
            $(this).addClass('active');
        } else {
            return false;
        }

        var sorting = $(this).data('sorting');

        $.ajax({
            method: 'POST',
            url: wpcatalog.ajaxurl,
            data: {
                action: 'wpcatalog_sorting_themes',
                sorting: sorting,
                slug: category_slug
            },
            beforeSend: function () {
                $('.main').addClass('loader');
            },
            success: function (response) {
                $('.main .row').html(response);

                if (category_slug === '/theme/' || category_slug.includes('/theme/page/')) {
                    history.pushState(null, null, '/theme/?sorting=' + sorting);
                } else {
                    history.pushState(null, null, '/category-theme/' + category_slug.split('/')[0] + '/?sorting=' + sorting);
                }
            },
            complete: function () {
                $('.main').removeClass('loader');
            }
        });
    });

    // ajax поиск
    $('.search-field').on('input', function () {
        var search = $(this).val();
        var result_block = $('.search-result');
        var category_slug = $('.select-tax').val();

        if (search.length > 1) {
            $.ajax({
                url: wpcatalog.ajaxurl,
                type: 'POST',
                data: {
                    action: 'ajax_search',
                    search: search,
                    slug: category_slug,
                },
                beforeSend: function () {
                    $('.search-form').addClass('loader-search');
                },
                success: function (result) {
                    result_block.addClass('active').html(result);
                    result_block.siblings().addClass('is-search');
                    $('.search-wrap').removeClass('active');
                },
                complete: function () {
                    $('.search-form').removeClass('loader-search');
                }
            });
        }
    });

    // поиск по категории
    $('.search-category-link').click(function (event) {
        event.preventDefault();

        var category_name = $(this).text().replace(/\s{2,}/g, ' ');
        var category_slug = $(this).attr('href').replace('/category-theme/', ' ');
        var category_fild = $('.select-category span');

        category_fild.text(category_name);

        $("select option[value=" + category_slug + "]").attr('selected', 'true').text(category_slug);

    });

    // перемещаем label в форме на странице контактов
    $('label[for=name]').detach().appendTo('.your-name');
    $('label[for=email]').detach().appendTo('.your-email');
    $('label[for=message]').detach().appendTo('.your-message');

    // подставляет название услуги в форму заявки на странице "Услуги"
    $('.service-button').click(function () {
        var title_service = $(this).siblings().find('.service-title').text();
        var form_subject = $('input[name="your-subject"]');
        var form_title = $('.modal-title');

        form_title.text(title_service);
        form_subject.val(title_service);
    });

}(jQuery) );