// Определяем константы Gulp
const {src, dest, parallel, series, watch} = require('gulp');

// Подключаем Browsersync
//const browserSync = require('browser-sync').create();

// Подключаем gulp-concat
const concat = require('gulp-concat');

// Подключаем gulp-uglify-es
const uglify = require('gulp-uglify-es').default;

// Подключаем Autoprefixer
const autoprefixer = require('gulp-autoprefixer');

// Подключаем модуль gulp-clean-css
const cleancss = require('gulp-clean-css');

function scripts() {
    return src([ // Берём файлы из источников
        'js/bootstrap.min.js', // Пример подключения библиотеки
        'js/main.js' // Пользовательские скрипты, использующие библиотеку, должны быть подключены в конце
    ])
        .pipe(concat('main.min.js')) // Конкатенируем в один файл
        .pipe(uglify()) // Сжимаем JavaScript
        .pipe(dest('js/')) // Выгружаем готовый файл в папку назначения
        //.pipe(browserSync.stream()) // Триггерим Browsersync для обновления страницы
}

function styles() {
    return src([ // Берём файлы из источников
        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
        'css/main.css',
        'css/responsive.css'
    ])
      //.pipe(eval(preprocessor)()) // Преобразуем значение переменной "preprocessor" в функцию
        .pipe(concat('main.min.css')) // Конкатенируем в файл app.min.js
        .pipe(autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true })) // Создадим префиксы с помощью Autoprefixer
        .pipe(cleancss( { level: { 1: { specialComments: 0 } }/* , format: 'beautify' */ } )) // Минифицируем стили
        .pipe(dest('css/')) // Выгрузим результат в папку "app/css/"
        //.pipe(browserSync.stream()) // Сделаем инъекцию в браузер
}

// Экспортируем функцию scripts() в таск scripts
exports.scripts = scripts;

// Экспортируем функцию styles() в таск styles
exports.styles = styles;

// Экспортируем дефолтный таск с нужным набором функций
exports.default = parallel(scripts, styles);