<?php get_header(); ?>
<?php get_template_part( 'template-parts/breadcrumbs' ); ?>

    <div class="page-content">
        <div class="container main-content">
            <main class="main">

				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

            </main>
        </div>
    </div>

<?php get_footer();
