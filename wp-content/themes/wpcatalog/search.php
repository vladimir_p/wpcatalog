<?php get_header(); ?>
<?php get_template_part( 'template-parts/breadcrumbs' ); ?>

    <div class="page-content">
        <section class="page-info-wrap">
            <div class="container">
                <div class="page-info">
                    <h2 class="page-title">
			            <?php printf( esc_html__( 'Результаты поиска для: %s', 'wpcatalog' ), '<span>' . get_search_query() . '</span>' ); ?>
                    </h2>
                </div>
            </div>
        </section>
        <div class="container main-content">
            <main class="main">
                <div class="row">
	                <?php if ( have_posts() ) : ?>

		                <?php
		                /* Start the Loop */
		                while ( have_posts() ) :
			                the_post();

			                get_template_part( 'template-parts/content', 'single' );

		                endwhile;

	                else :

		                get_template_part( 'template-parts/content', 'none' );

	                endif;
	                ?>
                </div>
                <div class="col-md-12">
                    <nav class="pagination">
                        <div class="nav-links">
				            <?php pagination();?>
                        </div>
                    </nav>
                </div>
            </main>
        </div>
    </div>

<?php get_footer();
