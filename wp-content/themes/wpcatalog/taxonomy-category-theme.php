<?php get_header(); ?>
<?php get_template_part( 'template-parts/breadcrumbs' ); ?>

	<div class="page-content">
        <section class="page-info-wrap">
            <div class="container">
                <div class="page-info">
                    <h1 class="page-title"><?php single_cat_title(); ?></h1>
                    <div class="page-desc"><?php echo category_description(); ?></div>
                </div>
                <div id="sorting-themes" class="sorting">
                    <span class="sorting-label">Сортировать по:</span>
                    <a href="#" class="button sorting-button <?php if ( !isset( $_GET['sorting'] ) ) echo 'active';  add_css_class( 'date' ); ?>" data-sorting="date">Дате добавления</a>
                    <a href="#" class="button sorting-button <?php add_css_class( 'views' ); ?>" data-sorting="views">Просмотрам</a>
                    <a href="#" class="button sorting-button <?php add_css_class( 'download' ); ?>" data-sorting="download">Скачиванию</a>
                </div>
            </div>
        </section>
		<div class="container main-content">
			<main class="main">
				<div class="row">
					<?php

					$term = get_queried_object();

					if (isset($_GET['sorting'])) {
						switch ( $_GET['sorting'] ) {
							case 'date':
								$theme_sorting  = '';
								$meta_value_num = 'date';
								break;
							case 'views':
								$theme_sorting  = 'theme_views';
								$meta_value_num = 'meta_value_num';
								break;
							case 'download':
								$theme_sorting  = 'theme_download';
								$meta_value_num = 'meta_value_num';
								break;
						}
					}

					$args = array(
						'post_status' => 'publish',
						'post_type'   => 'theme',
						'meta_key'    => isset( $theme_sorting ) ? $theme_sorting : '',
						'orderby'     => isset( $meta_value_num ) ? $meta_value_num : '',
						'category-theme' => $term->slug,
						'paged' => get_query_var('paged') ?: 1
					);

					$query = new WP_Query( $args );

					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) {
							$query->the_post();
							get_template_part( 'template-parts/content', 'single' );
						}
					} else {
						get_template_part( 'template-parts/content', 'none' );
					}

					?>
					<div class="col-md-12">
						<nav class="pagination">
							<div class="nav-links">
								<?php pagination();?>
							</div>
						</nav>
					</div>
				</div>
			</main>
		</div>
	</div>

<?php get_footer();
