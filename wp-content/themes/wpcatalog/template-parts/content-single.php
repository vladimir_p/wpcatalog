<?php
$category = get_the_terms($post->ID, 'category-theme');
$term_link  = get_term_link( (int) $category['0']->term_id, 'category-theme' );

?>
<div class="col-lg-4 col-sm-6">
	<article class="article">
        <div class="article-header">
            <h2 class="article-title"><?php the_title(); ?></h2>
            <h4 class="category-name">
                <a class="category-link" href="<?php echo $term_link; ?>"><?php echo $category['0']->name; ?></a>
            </h4>
        </div>
		<div class="article-block">
			<?php the_post_thumbnail(); ?>
			<div class="article-hover">
				<div class="article-desc"><?php the_excerpt(); ?></div>
				<div class="article-button">
					<a class="button" href="<?php the_permalink(); ?>">Подробнее</a>
					<a class="button" href="<?php the_field( 'link_demo' ); ?>" target="_blank" rel="nofollow">Demo</a>
				</div>
			</div>
		</div>
		<div class="article-info">
			<span class="article-view"><i class="fa fa-eye" aria-hidden="true"></i><?php get_theme_views($post->ID); ?></span>
			<span class="article-date"><i class="fa fa-calendar" aria-hidden="true"></i><?php the_time('d.m.Y'); ?></span>
			<span class="article-download"><i class="fa fa-download" aria-hidden="true"></i><?php get_theme_download($post->ID); ?></span>
		</div>
    </article>
</div>