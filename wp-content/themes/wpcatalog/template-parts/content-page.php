<div class="page-info">
    <h1 class="page-title"><?php the_title(); ?></h1>
</div>
<div class="page-content">
	<?php the_content(); ?>
</div>
