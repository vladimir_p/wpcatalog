<?php

$theme_version   = get_field( 'theme_version' );
$theme_developer = get_field( 'theme_developer' );
$category        = get_the_terms( $post->ID, 'category-theme' );
$term_link       = get_term_link( (int) $category['0']->term_id, 'category-theme' );
$monitor_image   = get_field( 'monitor_image' );
$tablet_image    = get_field( 'tablet_image' );
$mobile_image    = get_field( 'mobile_image' );

?>
<div class="wrap-page-title">
    <div class="container">
        <div class="row">
            <div class="col-xs-4">
                <h1 class="page-title"><?php the_title(); ?></h1>
            </div>
            <div class="col-xs-8">
                <div class="device-selections">
                    <i class="fa fa-desktop active"></i>
                    <i class="fa fa-tablet"></i>
                    <i class="fa fa-mobile"></i>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div id="template-device" class="device-monitor preview-template">
        <div class="preview-container">
            <img class="monitor-img" src="<?php echo $monitor_image['url']; ?>" alt="<?php echo $monitor_image['alt']; ?>">
            <img class="tablet-img" src="<?php echo $tablet_image['url']; ?>" alt="<?php echo $tablet_image['alt']; ?>">
            <img class="mobile-img" src="<?php echo $mobile_image['url']; ?>" alt="<?php echo $mobile_image['alt']; ?>">
        </div>
    </div>
</div>
<main class="main">
    <article class="article-single">
        <div class="container">
            <div class="article-single-button">
                <a class="button" target="_blank" rel="nofollow" href="<?php the_field( 'link_demo' ); ?>">Demo</a>
                <a id="theme-download" class="button" rel="nofollow" data-post-id="<?php the_id(); ?>" href="<?php the_field( 'link_download' ); ?>">Скачать</a>
            </div>
            <div class="article-content">
                <div class="tab-wrap">
                    <!-- active tab on page load gets checked attribute -->
                    <input type="radio" id="tab1" name="tabGroup1" class="tab" checked>
                    <label for="tab1">Информация</label>
                    <input type="radio" id="tab2" name="tabGroup1" class="tab">
                    <label for="tab2">Характеристики</label>
                    <input type="radio" id="tab3" name="tabGroup1" class="tab">
                    <label for="tab3">Описание</label>
                    <input type="radio" id="tab4" name="tabGroup1" class="tab">
                    <label for="tab4">Комментарии (<?php echo get_comments_number(); ?>)</label>
                    <div class="tab-content">
                        <table class="tableinfo">
                            <tbody>
                            <tr>
                                <td>Версия:</td>
                                <td><?php echo $theme_version; ?></td>
                            </tr>
                            <tr>
                                <td>Автор:</td>
                                <td>
                                    <a href="<?php echo $theme_developer['url']; ?>" target="_blank"
                                       rel="nofollow"><?php echo $theme_developer['title']; ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td>Категория:</td>
                                <td>
                                    <a href="<?php echo $term_link; ?>"><?php echo $category['0']->name; ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td>Просмотров:</td>
                                <td><?php get_theme_views( $post->ID ); ?></td>
                            </tr>
                            <tr>
                                <td>Закачек:</td>
                                <td><?php get_theme_download($post->ID); ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-content">
                        <table class="tableinfo">
                            <tbody>

							<?php
							if ( have_rows( 'theme_specifications' ) ) {
								while ( have_rows( 'theme_specifications' ) ) : the_row();
									?>

                                    <tr>
                                        <td><?php the_sub_field( 'theme_title' ); ?></td>
                                        <td><i class="fa fa-check"></i></td>
                                    </tr>

									<?php

								endwhile;
							}
							?>

                            </tbody>
                        </table>
                    </div>
                    <div class="tab-content"><?php the_content(); ?></div>
                    <div class="tab-content">

						<?php
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						?>

                    </div>
                </div>
            </div>
        </div>
    </article>
</main>
