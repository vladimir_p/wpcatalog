<div class="page-content">
    <section class="container main-content">
        <div class="page-info">
            <h2 class="page-title"><?php esc_html_e( 'Ничего не найдено', 'wpcatalog' ); ?></h2>
        </div>
    </section>
</div>
