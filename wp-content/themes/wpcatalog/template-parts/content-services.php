<div class="page-info">
	<h1 class="page-title"><?php the_title(); ?></h1>
</div>
<div class="services">
	<h2 class="services-title">Настройка сайта</h2>
	<div class="row services-row">
		<div class="col-lg-3 col-md-4 col-sm-6">
            <div class="service-wrap">
                <div class="service-block">
                    <div class="service-icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <h3 class="service-title">Установка шаблона</h3>
                    <p class="service-content">Вы можете заказать установку любого шаблона на свой сайт. С этого сайта или другого источника тем.</p>
                    <h3 class="service-price">200 грн</h3>
                </div>
                <button class="service-button" data-toggle="modal" data-target="#form-order">Заказать</button>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="service-wrap">
                <div class="service-block">
                    <div class="service-icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <h3 class="service-title">Установка плагина</h3>
                    <p class="service-content">При выборе этой услуги вы получите установку плагина и его настройку.</p>
                    <h3 class="service-price">100 грн</h3>
                </div>
                <button class="service-button" data-toggle="modal" data-target="#form-order">Заказать</button>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="service-wrap">
                <div class="service-block">
                    <div class="service-icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <h3 class="service-title">Повышение безопасности</h3>
                    <p class="service-content">Каждый год число атак на веб-сайты WordPress только растет. Чтобы защитить свой сайт и повысить безопасность, воспользуйтесь этой услугой.</p>
                    <h3 class="service-price">500 грн</h3>
                </div>
                <button class="service-button" data-toggle="modal" data-target="#form-order">Заказать</button>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="service-wrap">
                <div class="service-block">
                    <div class="service-icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <h3 class="service-title">Ускорение сайта</h3>
                    <p class="service-content">Оптимизация сайта на WordPress для Google PageSpeed.</p>
                    <h3 class="service-price">500 грн</h3>
                </div>
                <button class="service-button" data-toggle="modal" data-target="#form-order">Заказать</button>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-lg-push-3">
            <div class="service-wrap">
                <div class="service-block">
                    <div class="service-icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <h3 class="service-title">Локализация темы</h3>
                    <p class="service-content">Локализация темы на украинский или русский языки.</p>
                    <h3 class="service-price">Бесплатно</h3>
                </div>
                <button class="service-button" data-toggle="modal" data-target="#form-order">Заказать</button>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-lg-push-3">
            <div class="service-wrap">
                <div class="service-block">
                    <div class="service-icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <h3 class="service-title">Консультация</h3>
                    <p class="service-content">Вы можете задать любой вопрос касающейся вашего сайта. И получить на него ответ.</p>
                    <h3 class="service-price">Бесплатно</h3>
                </div>
                <button class="service-button" data-toggle="modal" data-target="#form-order">Заказать</button>
            </div>
        </div>
    </div>
    <h2 class="services-title">Разработка сайта</h2>
    <div class="row services-row">
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="service-wrap">
                <div class="service-block">
                    <div class="service-icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <h3 class="service-title">Landing Page</h3>
                    <p class="service-content">Лендинг пейдж направлена на решение одной задачи – призвать посетителя к действию.</p>
                    <h3 class="service-price">От 1000 грн</h3>
                </div>
                <button class="service-button" data-toggle="modal" data-target="#form-order">Заказать</button>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="service-wrap">
                <div class="service-block">
                    <div class="service-icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <h3 class="service-title">Сайт-визитка</h3>
                    <p class="service-content">Небольшой сайт на котором пользователи ознакомятся с информацией о компании, деятельности, контактах.</p>
                    <h3 class="service-price">От 2000 грн</h3>
                </div>
                <button class="service-button" data-toggle="modal" data-target="#form-order">Заказать</button>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="service-wrap">
                <div class="service-block">
                    <div class="service-icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <h3 class="service-title">Интернет магазин</h3>
                    <p class="service-content">Разработка магазина для продажи товаров через Интернет.Корзина, возможность создания категорий и карточки товаров, методов оплаты, экспорт и импорт данных.</p>
                    <h3 class="service-price">От 5000 грн</h3>
                </div>
                <button class="service-button" data-toggle="modal" data-target="#form-order">Заказать</button>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="service-wrap">
                <div class="service-block">
                    <div class="service-icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <h3 class="service-title">Блог</h3>
                    <p class="service-content">Личный блог – это отдельный сайт в интернете, на страницах которого Вы делитесь с пользователями интернета, полезными советами, навыками, рассказами, историями т.д. </p>
                    <h3 class="service-price">От 2000 грн</h3>
                </div>
                <button class="service-button" data-toggle="modal" data-target="#form-order">Заказать</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade form-order" id="form-order">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">

	            <?php echo do_shortcode('[contact-form-7 id="220" title="Форма заявки" html_class="form comment-form"]'); ?>

            </div>
        </div>
    </div>
</div>
