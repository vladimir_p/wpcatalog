<?php

if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php

	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
        <h4 class="comments-title">
			<?php
			$comment_count = get_comments_number();
			if ( '1' === $comment_count ) {
				echo '<i class="fa fa-comments-o" aria-hidden="true"></i> 1 комментарий';
			} else {
				echo '<i class="fa fa-comments-o" aria-hidden="true"></i> ' . $comment_count . ' комментариев';
			}
			?>
        </h4><!-- .comments-title -->

		<?php the_comments_navigation(); ?>

        <ul class="comments-list">
			<?php
			wp_list_comments(
				array(
					'style'      => 'ul',
					'callback'   => 'wpcatalog_comment',
				)
			);
			?>
        </ul><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
            <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'wpcatalog' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().

	$commenter = wp_get_current_commenter();

	$args = [
		'fields'               => [
			'author' => '<div class="comment-group">
			<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" required="required" />
		    <label for="author">' . __( 'Name' ) . ' <span class="required">*</span></label>
		</div>',
			'email'  => '<div class="comment-group">
			<input id="email" name="email" ' . 'type="email"' . ' value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" required="required" />
		    <label for="email">' . __( 'Email' ) . ' <span class="required">*</span></label> 
		</div>',
		],
		'comment_field'        => '<div class="comment-group">
		    <textarea id="comment" name="comment" rows="1" required="required"></textarea>
		    <label for="comment">' . _x( 'Comment', 'noun' ) . ' <span class="required">*</span></label>
	    </div>',
		'comment_notes_after'  => '',
		'id_form'              => 'comment-form',
		'id_submit'            => 'comment-submit',
		'class_form'           => 'comment-form',
		'class_submit'         => 'button',
		'name_submit'          => 'submit',
		'title_reply'          => __( 'Leave a Reply' ),
		'title_reply_to'       => __( 'Leave a Reply to %s' ),
		'title_reply_before'   => '<h4 id="reply-title" class="comment-title">',
		'title_reply_after'    => '</h4>',
		'cancel_reply_before'  => ' <small>',
		'cancel_reply_after'   => '</small>',
		'cancel_reply_link'    => __( 'Cancel reply' ),
		'label_submit'         => __( 'Отправить' ),
		'submit_button'        => '<input name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s" />',
		'submit_field'         => '<div class="form-submit">%1$s %2$s</div>',
		'format'               => 'xhtml',
		'comment_notes_before' => '',
	];

	comment_form( $args );

	?>

</div><!-- #comments -->
