<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">

	            <?php
                    if ( is_active_sidebar( 'footer-left' ) ) {
	                    dynamic_sidebar( 'footer-left' );
                    }
	            ?>

            </div>
            <div class="col-md-8">

	            <?php
	            if ( is_active_sidebar( 'footer-right' ) ) {
		            dynamic_sidebar( 'footer-right' );
	            }
	            ?>

            </div>
        </div>
    </div>
    <div class="copyright">© 2020 WPcatalog</div>
</footer>

<?php wp_footer(); ?>

<!-- rambler -->
<!-- Top100 (Kraken) Counter -->
<script>
    (function (w, d, c) {
        (w[c] = w[c] || []).push(function() {
            var options = {
                project: 7238908,
            };
            try {
                w.top100Counter = new top100(options);
            } catch(e) { }
        });
        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src =
            (d.location.protocol == "https:" ? "https:" : "http:") +
            "//st.top100.ru/top100/top100.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(window, document, "_top100q");
</script>
<noscript>
    <img src="//counter.rambler.ru/top100.cnt?pid=7238908" alt="Топ-100" />
</noscript>
<!-- END Top100 (Kraken) Counter -->

</body>
</html>
