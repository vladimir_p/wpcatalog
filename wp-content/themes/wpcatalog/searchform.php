<?php
$terms = get_terms( array(
	'hide_empty' => 0,
	'orderby'    => 'name',
	'order'      => 'ASC',
	'taxonomy'   => 'category-theme',
	'pad_counts' => 1
) );
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="search-wrap">
        <input class="search-field" type="search" placeholder="Найдите свою тему"
               value="<?php echo get_search_query(); ?>" name="s">
        <div class="search-field select-category">
            <span>Все темы</span>
            <i class="fa fa-angle-down"></i>
        </div>

        <select class="select-tax" name="category-theme">
            <option value="">Choose</option>
            <?php foreach ( $terms as $term ) { ?>
            <option value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
	        <?php } ?>
        </select>

        <button class="search-button">
            <span>Искать тему</span>
            <i class="fa fa-search"></i>
        </button>
        <div class="search-category">
            <ul class="search-list">

				<?php
				foreach ( $terms as $term ) {
					$icon_class = get_icon_category_theme( $term );
					?>
                    <li class="search-item">
                        <a class="category-link search-category-link" href="/category-theme/<?php echo $term->slug; ?>">
                            <i class="fa <?php echo $icon_class; ?>"></i>
                            <h3 class="category-name"><?php echo $term->name; ?></h3>
                        </a>
                    </li>
				<?php } ?>

            </ul>
        </div>
    </div>
    <ul class="search-result"></ul>
    <input type="hidden" name="post_type" value="theme" />
</form>