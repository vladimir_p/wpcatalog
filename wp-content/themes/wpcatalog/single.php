<?php get_header(); ?>
<?php get_template_part( 'template-parts/breadcrumbs' ); ?>

    <div class="page-content">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content' );

		endwhile; // End of the loop.
		?>

    </div>

<?php

get_footer();
